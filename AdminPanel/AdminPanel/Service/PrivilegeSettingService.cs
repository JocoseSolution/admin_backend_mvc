﻿using AdminPanel.Helper;
using AdminPanel.Models;
using System.Collections.Generic;

namespace AdminPanel.Service
{
    public static class PrivilegeSettingService
    {
        #region[Branch]
        internal static bool CreateBranch(string branchName, string userid)
        {
            return PrivilegeSettingHelper.CreateBranch(branchName, userid);
        }
        internal static List<Branch> GetBranchList()
        {
            return PrivilegeSettingHelper.GetBranchList();
        }
        internal static bool DeleteBranch(string id)
        {
            return PrivilegeSettingHelper.DeleteBranch(id);
        }

        internal static bool UpdateBranch(string id, string branchName, string userid)
        {
            return PrivilegeSettingHelper.UpdateBranch(id, branchName, userid);
        }
        #endregion

        #region[CityMaster]
        internal static string GetStates()
        {
            return PrivilegeSettingHelper.GetStates();
        }
        internal static List<CityMaster> GetCityList(string txtStateId, string txtCity)
        {
            return PrivilegeSettingHelper.GetCityList(txtStateId, txtCity);
        }
        internal static bool AddCity(string stateId, string cityName)
        {
            return PrivilegeSettingHelper.AddCity(stateId, cityName);
        }
        internal static bool UpdateCity(string id, string cityName)
        {
            return PrivilegeSettingHelper.UpdateCity(id, cityName);
        }
        internal static bool DeleteCity(string id)
        {
            return PrivilegeSettingHelper.DeleteCity(id);
        }
        #endregion

        #region[Executive]
        internal static string GetBranchList_forExecutive()
        {
            return PrivilegeSettingHelper.GetBranchList_forExecutive();
        }
        internal static string GetRoleList_forExecutive()
        {
            return PrivilegeSettingHelper.GetRoleList_forExecutive();
        }
        internal static List<Executive> GetExecutiveList()
        {
            return PrivilegeSettingHelper.GetExecutiveList();
        }
        internal static bool AddExecutive(Executive model)
        {
            return PrivilegeSettingHelper.AddExecutive(model);
        }
        internal static bool DeleteExecutive(string userId)
        {
            return PrivilegeSettingHelper.DeleteExecutive(userId);
        }
        internal static bool UpdateCity(string user_id, string newPassword, string newRoleId, string newStatus)
        {
            return PrivilegeSettingHelper.UpdateCity(user_id, newPassword, newRoleId, newStatus);
        }
        #endregion
    }
}