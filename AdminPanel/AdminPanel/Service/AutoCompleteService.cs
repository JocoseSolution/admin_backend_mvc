﻿using AdminPanel.Helper;
using AdminPanel.Models;
using System.Collections.Generic;

namespace AdminPanel.Service
{
    public static class AutoCompleteService
    {
        public static List<AutoAgency> GetAgencyAutoSearch(string inputPara)
        {
            ExecuRegister lu = new ExecuRegister();
            bool isLogedIn = AdministrationService.IsExecutiveLogin(ref lu);
            return AutoCompleteHelper.GetAgencyAutoSearch(inputPara, lu.user_type, lu.user_id);
        }
        public static List<AutoAirline> GetAirlineSearch(string inputPara)
        {            
            return AutoCompleteHelper.GetAirlineSearch(inputPara);
        }
        public static List<string> GetNotificationDetails()
        {
            List<string> result = new List<string>();
            AutoNotification notification = AutoCompleteHelper.GetNotificationDetails();

            int count = 0;
            string notify = string.Empty;
            if (notification.TicketCancelList != null)
            {
                notify += "<li class='nav-item' style='color: #D8000C;background-color: #FFBABA;border: 1px solid #ff6e6e;'><span>Ticket Cancel Details</span><ul class='nav-item' style='margin-left: -50px;'>";
                foreach (var item in notification.TicketCancelList)
                {
                    notify += "<li style='padding: 2px;'><a class='dropdown-item' href=''><span class='message'>OrderId : " + item.OrderId + " [" + item.Agency_Name + "]</span></a></li>";
                    count = count + 1;
                }
                notify += "</ul></li>";
            }
            if (notification.TicketHoldList != null)
            {
                notify += "<li class='nav-item' style='color: #00529B;background-color: #BDE5F8;border: 1px solid #78caf1;'><span>Ticket Hold Details</span><ul class='nav-item' style='margin-left: -50px;'>";
                foreach (var item in notification.TicketHoldList)
                {
                    notify += "<li style='padding: 2px;'><a class='dropdown-item' href=''><span class='message'>OrderId : " + item.OrderId + " [" + item.Agency_Name + "]</span></a></li>";
                    count = count + 1;
                }
                notify += "</ul></li>";
            }
            if (notification.TicketReissueList != null)
            {
                notify += "<li class='nav-item' style='color: #4F8A10;background-color: #DFF2BF;border: 1px solid #bfe57f;'><span>Ticket Reissue Details</span><ul class='nav-item' style='margin-left: -50px;'>";
                foreach (var item in notification.TicketReissueList)
                {
                    notify += "<li style='padding: 2px;'><a class='dropdown-item' href=''><span class='message'>OrderId : " + item.OrderId + " [" + item.Agency_Name + "]</span></a></li>";
                    count = count + 1;
                }
                notify += "</ul></li>";
            }
            result.Add(notify);
            result.Add(count.ToString()); ;

            return result;
        }
    }
}