﻿using AdminPanel.Models;
using AdminPanel.Service;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AdminPanel.Controllers
{
    public class FlightSettingController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        #region[FareTypeMaster]
        public ActionResult FareTypeMaster(FareTypeMaster model)
        {
            List<FareTypeMaster> list = new List<FareTypeMaster>();

            if (model.Id > 0)
            {
                Models.ExecuRegister lu = ((List<Models.ExecuRegister>)HttpContext.Session["executive"])[0];
                model.UpdatedBy = lu.user_id;
                bool isSuccess = FlightSettingService.UpdateFareTypeMaster(model);
                if (isSuccess)
                {
                    ViewBag.UpdateMsg = "Data Updated Successfully";
                }
            }
            list = FlightSettingService.FareTypeMasterList();
            return View(list);
        }
        #endregion
    }
}