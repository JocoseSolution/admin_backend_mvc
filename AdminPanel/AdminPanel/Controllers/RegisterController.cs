﻿using AdminPanel.Models;
using AdminPanel.Service;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AdminPanel.Controllers
{
    public class RegisterController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        #region [Group Type Details]
        public ActionResult GroupTypeDetails(string agentType, string description)
        {
            List<AgentType> list = new List<AgentType>();
            try
            {
                if (!string.IsNullOrEmpty(agentType) && !string.IsNullOrEmpty(description))
                {
                    bool IsSuccess = RegisterService.InsertGroupTypeDetails(agentType, description);
                }
                list = RegisterService.GroupTypeDetailsList();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(list);
        }
        public ActionResult UpdateGroupTypeDetails(string actionType, string userType, string desc = null)
        {
            if (actionType == "edit")
            {
                List<string> strList = new List<string>() { userType, desc };
                return View(strList);
            }

            bool isSuccess = RegisterService.UpdateGroupTypeDetails(actionType, userType, desc);

            return RedirectToAction("GroupTypeDetails", "Register");
        }
        #endregion
    }
}