﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace AdminPanel.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region [Master Page]
            bundles.Add(new StyleBundle(
                "~/master/css").Include("~/Content/design/css/bootstrap.min.css",
                "~/Content/design/css/nprogress.css",
                "~/Content/design/css/jquery.mCustomScrollbar.min.css",
                "~/Content/design/css/custom.min.css"));

            bundles.Add(new ScriptBundle("~/master/js").Include(
                "~/Content/design/js/jquery.min.js",
                "~/Content/design/js/bootstrap.bundle.min.js",
                "~/Content/design/js/fastclick.js",
                "~/Content/design/js/nprogress.js",
                "~/Content/js/jquery.smartWizard.js",
                "~/Content/design/js/jquery.mCustomScrollbar.concat.min.js",
                "~/Content/design/js/custom.min.js", "~/Content/js/custom/masternotification.js"));
            #endregion

            #region "Date Picker"
            bundles.Add(new StyleBundle("~/date/css").Include("~/Content/date/css/datepicker.css"));
            bundles.Add(new ScriptBundle("~/date/js").Include(                
                "~/Content/date/js/datepicker.js",
                "~/Content/date/js/datemain.js"));
            #endregion

            #region "pnotify"
            bundles.Add(new StyleBundle("~/pnotify/css").Include("~/Content/date/css/pnotify/css/pnotify.css",
                "~/Content/date/css/pnotify/css/pnotify.buttons.css",
                "~/Content/date/css/pnotify/css/pnotify.nonblock.css"));
            bundles.Add(new ScriptBundle("~/pnotify/js").Include(
                "~/Content/date/css/pnotify/js/icheck.min.js",
                "~/Content/date/css/pnotify/js/pnotify.js",
                "~/Content/date/css/pnotify/js/pnotify.buttons.js",
                "~/Content/date/css/pnotify/js/pnotify.buttons.js"));
            #endregion

            #region "Auto Complete"
            bundles.Add(new StyleBundle("~/autocomplete/css").Include("~/Content/design/css/custom/autocomplete.css"));
            bundles.Add(new ScriptBundle("~/autocomplete/js").Include("~/Content/js/custom/autocomplete-jquery-ui.js"));
            #endregion

            #region "select2"
            bundles.Add(new StyleBundle("~/select2/css").Include("~/Content/design/dropdown_search_box/css/select2.min.css"));
            bundles.Add(new ScriptBundle("~/select2/js").Include("~/Content/design/dropdown_search_box/js/select2.min.js"));
            #endregion
        }
    }
}