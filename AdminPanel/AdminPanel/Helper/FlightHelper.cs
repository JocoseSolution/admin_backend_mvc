﻿using AdminPanel.DataBase;
using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace AdminPanel.Helper
{
    public static class FlightHelper
    {
        public static FlightModel GetTicketReport(string userType, string loginID, FlightModel model)
        {
            FlightModel result = new FlightModel();

            try
            {
                DataTable dtReport = FlightDatabase.GetTicketReport(userType, loginID, model);
                if (dtReport != null && dtReport.Rows.Count > 0)
                {
                    List<FlightModel> ticketReportList = new List<FlightModel>();
                    for (int i = 0; i < dtReport.Rows.Count; i++)
                    {
                        FlightModel flight = new FlightModel();
                        flight.TotalAfterDis = !string.IsNullOrEmpty(dtReport.Rows[i]["TotalAfterDis"].ToString()) ? dtReport.Rows[i]["TotalAfterDis"].ToString() : "- - -";
                        flight.TotalFare = !string.IsNullOrEmpty(dtReport.Rows[i]["TotalFare"].ToString()) ? dtReport.Rows[i]["TotalFare"].ToString() : "- - -";
                        flight.OrderId = !string.IsNullOrEmpty(dtReport.Rows[i]["OrderId"].ToString()) ? dtReport.Rows[i]["OrderId"].ToString() : "- - -";
                        flight.PgEmail = !string.IsNullOrEmpty(dtReport.Rows[i]["PgEmail"].ToString()) ? dtReport.Rows[i]["PgEmail"].ToString() : "- - -";
                        flight.PgMobile = !string.IsNullOrEmpty(dtReport.Rows[i]["PgMobile"].ToString()) ? dtReport.Rows[i]["PgMobile"].ToString() : "- - -";
                        flight.TotalBookingCost = !string.IsNullOrEmpty(dtReport.Rows[i]["TotalBookingCost"].ToString()) ? dtReport.Rows[i]["TotalBookingCost"].ToString() : "- - -";
                        flight.Sector = !string.IsNullOrEmpty(dtReport.Rows[i]["sector"].ToString()) ? dtReport.Rows[i]["sector"].ToString() : "- - -";
                        flight.AgentId = !string.IsNullOrEmpty(dtReport.Rows[i]["AgentId"].ToString()) ? dtReport.Rows[i]["AgentId"].ToString() : "- - -";
                        flight.AgencyName = !string.IsNullOrEmpty(dtReport.Rows[i]["AgencyName"].ToString()) ? dtReport.Rows[i]["AgencyName"].ToString() : "- - -";
                        flight.AgentType = !string.IsNullOrEmpty(dtReport.Rows[i]["AgentType"].ToString()) ? dtReport.Rows[i]["AgentType"].ToString() : "- - -";
                        flight.PaxId = !string.IsNullOrEmpty(dtReport.Rows[i]["PaxId"].ToString()) ? dtReport.Rows[i]["PaxId"].ToString() : "- - -";
                        flight.PaxType = !string.IsNullOrEmpty(dtReport.Rows[i]["PaxType"].ToString()) ? dtReport.Rows[i]["PaxType"].ToString() : "- - -";
                        flight.Status = !string.IsNullOrEmpty(dtReport.Rows[i]["Status"].ToString()) ? dtReport.Rows[i]["Status"].ToString() : "- - -";
                        flight.CreateDate = !string.IsNullOrEmpty(dtReport.Rows[i]["CreateDate"].ToString()) ? UtilityClass.DisplayDateFormate(dtReport.Rows[i]["CreateDate"].ToString()) : "- - -";
                        flight.VC = !string.IsNullOrEmpty(dtReport.Rows[i]["VC"].ToString()) ? dtReport.Rows[i]["VC"].ToString() : "- - -";
                        flight.AirlineLogo = !string.IsNullOrEmpty(dtReport.Rows[i]["airlinelogo"].ToString()) ? dtReport.Rows[i]["airlinelogo"].ToString() : "- - -";
                        flight.GdsPnr = !string.IsNullOrEmpty(dtReport.Rows[i]["GdsPnr"].ToString()) ? dtReport.Rows[i]["GdsPnr"].ToString() : "- - -";
                        flight.TicketNumber = !string.IsNullOrEmpty(dtReport.Rows[i]["TicketNumber"].ToString()) ? dtReport.Rows[i]["TicketNumber"].ToString() : "- - -";
                        flight.FName = !string.IsNullOrEmpty(dtReport.Rows[i]["FName"].ToString()) ? dtReport.Rows[i]["FName"].ToString() : "- - -";
                        flight.LName = !string.IsNullOrEmpty(dtReport.Rows[i]["LName"].ToString()) ? dtReport.Rows[i]["LName"].ToString() : "- - -";
                        flight.ExecutiveId = !string.IsNullOrEmpty(dtReport.Rows[i]["ExecutiveId"].ToString()) ? dtReport.Rows[i]["ExecutiveId"].ToString() : "- - -";
                        flight.Trip = !string.IsNullOrEmpty(dtReport.Rows[i]["Trip"].ToString()) ? dtReport.Rows[i]["Trip"].ToString() : "- - -";
                        flight.RejectedRemark = !string.IsNullOrEmpty(dtReport.Rows[i]["RejectedRemark"].ToString()) ? dtReport.Rows[i]["RejectedRemark"].ToString() : "- - -";
                        flight.Provider = !string.IsNullOrEmpty(dtReport.Rows[i]["Provider"].ToString()) ? dtReport.Rows[i]["Provider"].ToString() : "- - -";
                        flight.FareType = !string.IsNullOrEmpty(dtReport.Rows[i]["FareType"].ToString()) ? dtReport.Rows[i]["FareType"].ToString() : "- - -";
                        flight.PartnerName = !string.IsNullOrEmpty(dtReport.Rows[i]["PartnerName"].ToString()) ? dtReport.Rows[i]["PartnerName"].ToString() : "- - -";
                        flight.FTYPE = !string.IsNullOrEmpty(dtReport.Rows[i]["FTYPE"].ToString()) ? dtReport.Rows[i]["FTYPE"].ToString() : "- - -";
                        flight.JourneyDate = !string.IsNullOrEmpty(dtReport.Rows[i]["JourneyDate"].ToString()) ? dtReport.Rows[i]["JourneyDate"].ToString() : "- - -";
                        flight.PaymentMode = !string.IsNullOrEmpty(dtReport.Rows[i]["PaymentMode"].ToString()) ? dtReport.Rows[i]["PaymentMode"].ToString() : "- - -";
                        flight.CountFound = !string.IsNullOrEmpty(dtReport.Rows[i]["countFound"].ToString()) ? dtReport.Rows[i]["countFound"].ToString() : "- - -";
                        flight.PgCharges = !string.IsNullOrEmpty(dtReport.Rows[i]["PgCharges"].ToString()) ? dtReport.Rows[i]["PgCharges"].ToString() : "- - -";
                        flight.PName = !string.IsNullOrEmpty(dtReport.Rows[i]["PName"].ToString()) ? dtReport.Rows[i]["PName"].ToString() : "- - -";
                        flight.FareRule = !string.IsNullOrEmpty(dtReport.Rows[i]["FareRule"].ToString()) ? dtReport.Rows[i]["FareRule"].ToString() : "- - -";
                        ticketReportList.Add(flight);
                    }
                    result.TicketReportList = ticketReportList;
                    result.TotalCount = ticketReportList.Count;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
    }
}