﻿namespace AdminPanel.Models
{
    public class AgentType
    {
        public int Counter { get; set; }
        public string GroupType { get; set; }
        public string UploadCategory { get; set; }
        public string UploadCategoryText { get; set; }
        public string SubCategory { get; set; }
        public string Text { get; set; }
        public string CreatedDate { get; set; }
        public string Distr { get; set; }
    }
}