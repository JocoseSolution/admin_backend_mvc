﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace AdminPanel.Models
{
    public static class CommonClass
    {
        public static List<SelectListItem> CommonPopulateList(Dictionary<string, string> pairList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in pairList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.Key.ToString(),
                    Value = item.Value.ToString()
                });
            }

            return items;
        }
    }
}