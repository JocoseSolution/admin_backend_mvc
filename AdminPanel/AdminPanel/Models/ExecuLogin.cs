﻿using System.Collections.Generic;

namespace AdminPanel.Models
{
    public class ExecuRegister
    {
        public int counter { get; set; }
        public string user_id { get; set; }
        public string password { get; set; }
        public int role_id { get; set; }
        public string role_type { get; set; }
        public string user_type { get; set; }
        public string type_id { get; set; }
        public string TripExec { get; set; }
        public bool DExport { get; set; }
        public string ModeTypeITZ { get; set; }
        public string MchntKeyITZ { get; set; }
        public string SvcTypeITZ { get; set; }
        public string role_name { get; set; }
        public string name { get; set; }
        public string email_id { get; set; }
        public string mobile_no { get; set; }
        public bool status { get; set; }
        public string trip { get; set; }
        public string Branch { get; set; }
        public string RedirectUrl { get; set; }
    }
    public class MenuList
    {
        public int page_id { get; set; }
        public string Page_name { get; set; }
        public string Page_url { get; set; }
        public string Is_Parent_Page { get; set; }
        public int Root_page_id { get; set; }
        public string ActiveClass { get; set; }
        public bool MainMenu { get; set; }
        public List<MenuList> ExecMenuList { get; set; }
    }
}