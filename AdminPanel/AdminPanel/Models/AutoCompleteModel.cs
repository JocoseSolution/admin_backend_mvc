﻿using System.Collections.Generic;

namespace AdminPanel.Models
{
    public class AutoAgency
    {
        public string Agency_Name { get; set; }
        public string User_Id { get; set; }
    }
    public class AutoAirline
    {
        public string AirlineName { get; set; }
        public string AirlineCode { get; set; }
    }

    public class AutoNotification
    {
        public string OrderId { get; set; }
        public string Url { get; set; }
        public string Agency_Name { get; set; }
        public List<AutoNotification> TicketCancelList { get; set; }
        public List<AutoNotification> TicketHoldList { get; set; }
        public List<AutoNotification> TicketReissueList { get; set; }
    }
}