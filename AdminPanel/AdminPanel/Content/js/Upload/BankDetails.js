﻿////$("#txtBankName").val(null);
////$("#txtBranchName").val(null);
////$("#txtArea").val(null);


$("#reset").click(function () {
    ClearFields();
});

function ClearFields() {
    $("#bankname").val("");
    $("#branchname").val("");
    $("#area").val("");
    $("#accNumber").val("");
    $("#ifsc").val("");
    $("#distributer").val("");
}

$("#submit").click(function () {

    if (CheckFocusBlankValidation("bankname")) return !1;
    if (CheckFocusBlankValidation("branchname")) return !1;
    if (CheckFocusBlankValidation("area")) return !1;
    if (CheckFocusBlankValidation("accNumber")) return !1;
    if (CheckFocusBlankValidation("ifsc")) return !1;
    if (CheckFocusDropDownBlankValidation("distributer")) return !1;


    let model = {};

    model.BankName = $("#bankname").val();
    model.BranchName = $("#branchname").val();
    model.Area = $("#area").val();
    model.AccountNumber = $("#accNumber").val();
    model.NEFTCode = $("#ifsc").val();
    model.DISTRID = $("#distributer").val();


    $.ajax({
        type: "Post",
        url: "/Upload/AddBankDetails",
        data: '{model:' + JSON.stringify(model) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data == true) {
                alert("Data has been added successfully");
                ClearFields();
                location.reload();
            }
        }
    });
});

function DeleteBankDetails(id) {
    let isDelete = confirm("Are you sure want to delete this bank detail");

    if (isDelete) {

        $.ajax({
            type: "Post",
            url: "/Upload/DeleteBankDetails",
            data: '{id:' + JSON.stringify(id) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data == true) {
                    alert("Data has been deleted successfully");
                    ClearFields();
                    $("#" + id).remove();
                    datacount = datacount - 1;
                    if (datacount == 0) {
                        $("tbody").html('<tr style="text-align:center"><td colspan = "6" style = "color:red;">No Record Found</td></tr>');
                    }
                }
            }
        });
    }

}