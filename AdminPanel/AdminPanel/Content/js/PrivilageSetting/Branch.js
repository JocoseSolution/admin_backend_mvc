﻿let editCount = 0;
$("#submit").click(function () {

    if (CheckFocusBlankValidation("branchname")) return !1;

    let branchName = $("#branchname").val();

    $.ajax({
        type: "Post",
        url: "/PrivilegeSetting/CreateBranch",
        data: '{branchName:' + JSON.stringify(branchName) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data == true) {
                $("#updateStatus").html("Branch has been updated successfully").css("color", "green");
                location.reload();
            }
            else {
                $("#updateStatus").html("Branch already exist").css("color", "red");
            }
            $("#branchname").val("");
        }
    });
});
function DeleteBranch(id) {
    let isDelete = confirm("Are you sure want to delete this Branch");

    if (isDelete) {

        $.ajax({
            type: "Post",
            url: "/PrivilegeSetting/DeleteBranch",
            data: '{id:' + JSON.stringify(id) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data == true) {
                    alert("Data has been deleted successfully");
                    $("#branchname").val("");
                    $("#" + id).remove();
                    datacount = datacount - 1;
                    if (datacount == 0) {
                        $("tbody").html('<tr style="text-align:center"><td colspan = "6" style = "color:red;">No Record Found</td></tr>');
                    }
                }
            }
        });
    }

}
function EditBranch(id) {

    if (editCount > 0) {
        return alert("Cannot edit multiple fields.\nClose open fields first.");
    }

    let oldBranchName = $("#branchName_" + id).html();

    $("#branchName_" + id).html('<input type="text" id="branchname_edit" value=\'' + oldBranchName + '\'>');
    $("#branchAction_" + id).html('<u style="color:red" onclick="UpdateBranch(' + id + ')">Update</u>&nbsp;&nbsp;&nbsp;&nbsp;<u style="color:red" onclick="CancelEditing(' + id + ',\'' + oldBranchName + '\')">Cancel</u>');

    editCount++;
}
function CancelEditing(id, oldBranchName) {
    $("#branchName_" + id).html(oldBranchName);
    $("#branchAction_" + id).html('<u style="color:red" onclick="EditBranch(' + id + ')">Edit</u>&nbsp;&nbsp;&nbsp;&nbsp;<u style="color:red" onclick="DeleteBranch(' + id + ')">Delete</u>');
    editCount--;
}
function UpdateBranch(id) {

    if (CheckFocusBlankValidation("branchname_edit")) return !1;

    let branchName = $("#branchname_edit").val();

    $.ajax({
        type: "Post",
        url: "/PrivilegeSetting/UpdateBranch",
        data: '{branchName:' + JSON.stringify(branchName) + ',id:' + JSON.stringify(id) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data == true) {
                $("#updateStatus").html("Branch has been updated successfully").css("color", "green");
                location.reload();
            }
            else {
                $("#updateStatus").html("Branch updation failed").css("color", "red");
                $("#branchAction_" + id).html('<u style="color:red" onclick="EditBranch(' + id + ')">Edit</u>&nbsp;&nbsp;&nbsp;&nbsp;<u style="color:red" onclick="DeleteBranch(' + id + ')">Delete</u>');
            }
            $("#branchname").val("");
        }
    });
};