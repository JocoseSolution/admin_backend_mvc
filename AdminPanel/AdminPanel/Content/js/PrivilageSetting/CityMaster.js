﻿
$("#addCityBtn").click(function () {

    if (CheckFocusDropDownBlankValidation("ddlState")) return !1;
    if (CheckFocusBlankValidation("txtCity")) return !1;

    let cityName = $("#txtCity").val();
    let stateId = $("#ddlState option:selected").val();

    $.ajax({
        type: "Post",
        url: "/PrivilegeSetting/AddCity",
        data: '{stateId:' + JSON.stringify(stateId) + ',cityName:' + JSON.stringify(cityName) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data == true) {
                $("#updateStatus").html("City has been updated successfully").css("color", "green");
                location.reload();
            }
            else {
                $("#updateStatus").html("City already exist").css("color", "red");
            }
            $("#branchname").val("");
        }
    });

});

let editCount = 0;

function EditCity(id) {

    if (editCount > 0) {
        return alert("Cannot edit multiple fields.\nClose open fields first.");
    }

    let oldCityName = $("#cityname_" + id).val();
    $("#cityname_" + id).removeAttr("readonly").css("border", "1px solid black");
    $("#Action_" + id).html('<u style="color:red" onclick="UpdateCity(' + id + ',\'' + oldCityName + '\')">Update</u>&nbsp;&nbsp;&nbsp;&nbsp;<u style="color:red" onclick="CancelEditing(' + id + ',\'' + oldCityName + '\')">Cancel</u>');

    editCount++;
}
function CancelEditing(id, oldCityName) {
    $("#cityname_" + id).val(oldCityName);
    $("#cityname_" + id).prop("readonly", true).css("border", "none");
    $("#Action_" + id).html('<u style="color:red" onclick="EditCity(' + id + ')">Edit</u>&nbsp;&nbsp;&nbsp;&nbsp;<u style="color:red" onclick="DeleteCity(' + id + ')">Delete</u>');
    editCount--;
}
function UpdateCity(id, oldCityName) {

    if ($("#cityname_" + id).val() == '') {
        $("#cityname_" + id).css("border", "1px solid red");
        return !1;
    }
    else {
        $("#cityname_" + id).css("border", "none");
    }

    let cityName = $("#cityname_" + id).val();

    $.ajax({
        type: "Post",
        url: "/PrivilegeSetting/UpdateCity",
        data: '{cityName:' + JSON.stringify(cityName) + ',id:' + JSON.stringify(id) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data == true) {
                $("#updateStatus").html("City has been updated successfully").css("color", "green");
            }
            else {
                $("#updateStatus").html("City updation failed").css("color", "red");
                $("#cityname_" + id).val(oldCityName);
            }
            $("#Action_" + id).html('<u style="color:red" onclick="EditCity(' + id + ')">Edit</u>&nbsp;&nbsp;&nbsp;&nbsp;<u style="color:red" onclick="DeleteCity(' + id + ')">Delete</u>');

        }
    });
};

function DeleteCity(id) {
    let isDelete = confirm("Are you sure want to delete this city ?");

    if (isDelete) {

        $.ajax({
            type: "Post",
            url: "/PrivilegeSetting/DeleteCity",
            data: '{id:' + JSON.stringify(id) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data == true) {
                    alert("Data has been deleted successfully");
                    $("#branchname").val("");
                    $("#" + id).remove();
                    datacount = datacount - 1;
                    if (datacount == 0) {
                        $("tbody").html('<tr style="text-align:center"><td colspan = "6" style = "color:red;">No Record Found</td></tr>');
                    }
                }
            }
        });
    }

}