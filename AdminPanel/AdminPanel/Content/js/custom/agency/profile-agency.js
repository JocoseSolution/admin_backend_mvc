﻿$(".agencydel").click(function () {
    var $buttonClicked = $(this);
    var agencyuid = $buttonClicked.attr('data-agencyuid');
    $("#Agency_" + agencyuid).html("<i class='fa fa-spinner fa-pulse'></i>");
    var options = { "backdrop": "static", keyboard: true };
    $.ajax({
        type: "GET",
        url: "/Profile/AgencyDetailById",
        contentType: "application/json; charset=utf-8",
        data: { "userid": agencyuid },
        datatype: "json",
        success: function (data) {
            $('#AgencyContent').html(data);
            $('#AgencyDelModel').modal(options);
            $('#AgencyDelModel').modal('show');
            $("#Agency_" + agencyuid).html(agencyuid);
        },
        error: function () {
            alert("Dynamic content load failed.");
            $("#Agency_" + agencyuid).html(agencyuid);
        }
    });
});

GetCityList = (tid, type) => {
    let stateid = "";
    let ddlId = "";
    if (tid == 0) {
        stateid = $("#State option:selected").val();
        ddlId = "#City";
    }
    else {
        stateid = $("#GST_State option:selected").val();
        ddlId = "#GST_City";
    }

    if (type == "create") {
        if (tid == 0) {
            stateid = $("#ddlState option:selected").val();
            ddlId = "#ddlCity";
        }
        else {
            stateid = $("#ddlGstState option:selected").val();
            ddlId = "#ddlGstCity";
        }
    }

    $(ddlId).prop("disabled", true);
    $(ddlId).children().remove();
    $(ddlId).append($('<option>').val(0).text('Please wait...'));
    if (stateid != "") {
        $.ajax({
            type: "GET",
            url: "/Profile/GetCityList",
            contentType: "application/json; charset=utf-8",
            data: { "stateid": stateid },
            datatype: "json",
            success: function (data) {
                $(ddlId).children().remove();
                $(ddlId).append($('<option>').val(0).text('Select City'));
                $.each(data, function (i, data) {
                    $(ddlId).append($('<option></option>').val(data.Value).html(data.Text));
                });
                $(ddlId).prop("disabled", false);
            }
        });
    }
    else {
        $(ddlId).append($('<option>').val(0).text('Select City'));
    }
}

SearchAgencyDetl = () => {
    $("#btnSearch").html("Searching...<i class='fa fa-spinner fa-pulse'></i>");
}

UpdateAgencyDetl = () => {
    if (CheckFocusBlankValidation("Address")) return !1;
    if (CheckFocusBlankValidation("Agency_Name")) return !1;
    if (CheckFocusDropDownBlankValidation("State")) return !1;
    if (CheckFocusDropDownBlankValidation("City")) return !1;
    if (CheckFocusBlankValidation("zipcode")) return !1;

    if (CheckFocusBlankValidation("Fname")) return !1;
    if (CheckFocusBlankValidation("Lname")) return !1;
    if (CheckFocusBlankValidation("Mobile")) return !1;
    if (CheckFocusBlankValidation("Email")) return !1;
    if (CheckEmailValidatoin("Email")) return !1;

    if (CheckFocusBlankValidation("NamePanCard")) return !1;
    if (CheckFocusBlankValidation("PanNo")) return !1;
    

    var model = {};
    model.User_Id = $("#User_Id").val();
    model.PWD = $("#PWD").val();
    model.Balance = $("#Balance").val();
    model.AgentLimit = $("#AgentLimit").val();
    model.DueAmount = $("#DueAmount").val();
    model.Address = $("#Address").val();
    model.Agency_Name = $("#Agency_Name").val();
    model.City = $("#City option:selected").val();
    model.State = $("#State option:selected").val();
    model.zipcode = $("#zipcode").val();
    model.Country = $("#Country").val();
    model.Title = $("#Title").val();
    model.Fname = $("#Fname").val();
    model.Lname = $("#Lname").val();
    model.Mobile = $("#Mobile").val();
    model.Email = $("#Email").val();
    model.NamePanCard = $("#NamePanCard").val();
    model.PanNo = $("#PanNo").val();
    model.Fax_no = ""; //$("#Fax_no").val();
    model.GSTNO = $("#GSTNO").val();
    if (model.GSTNO != "") {
        model.Is_GST_Apply = "1";
    }
    else {
        model.Is_GST_Apply = "0";
    }
    model.GST_Company_Name = $("#GST_Company_Name").val();
    model.GST_Company_Address = $("#GST_Company_Address").val();
    model.GST_PhoneNo = $("#GST_PhoneNo").val();
    model.GST_Email = $("#GST_Email").val();
    model.GST_Pincode = $("#GST_Pincode").val();
    model.GST_State = $("#GST_State").val();
    model.GST_City = $("#GST_City").val();
    model.Agent_Type = $("#ddlAgent_Type option:selected").val();
    if (model.Agent_Type == "") {
        model.Agent_Type = $("#Agent_Type").val();
    }
    model.SalesExecID = $("#SalesExecID option:selected").val();
    model.Agent_status = $("#Agent_status option:selected").val();
    model.Online_Tkt = $("#Online_Tkt option:selected").val();
    model.AgentLimit = $("#AgentLimit").val();
    model.IrctcId = $("#IrctcId").val();
    model.Distr = $("#ddlDistr option:selected").val();
    if (model.Distr == "") {
        model.Distr = $("#Distr").val();
    }
    if ($("#OTPLoginStatus").prop("checked") == true) {
        model.OTPLoginStatus = true;
    }
    else {
        model.OTPLoginStatus = false;
    }
    if ($("#PasswordExpMsg").prop("checked") == true) {
        model.PasswordExpMsg = true;
    }
    else {
        model.PasswordExpMsg = false;
    }
    $("#btnAgencyUpdate").html("Updating...<i class='fa fa-spinner fa-pulse'></i>");
    $.ajax({
        type: "Post",
        url: "/Profile/UpdateAgencyDel",
        contentType: "application/json; charset=utf-8",
        data: '{model: ' + JSON.stringify(model) + '}',
        datatype: "json",
        success: function (data) {
            $(".modal-footer").css("display", "none");
            if (data == "success") {
                //$(".close").css("display", "none");
                $("#UpdateAgencyBody").html("").html("<div style='text-align: center;'><p><i class='fa fa-check-circle text-success' aria-hidden='true' style='font-size: 200px;'></i><br></p><h4 class='text-success'>Agency update successfully.</h4><p></p></div>")
                //window.setInterval(function () { window.location.reload() }, 1000);
            }
            else {
                $("#UpdateAgencyBody").html("").html("<div style='text-align: center;'><p><i class='fa fa-times-circle text-danger' aria-hidden='true' style='font-size: 200px;'></i><br></p><h4 class='text-danger'>There is some error occured.</h4><p></p></div>")
            }
        }
    });
}

$("#chkIsGst").click(function () {
    if ($(this).prop("checked") == true) {
        $("#DivGstSection").css("display", "block");
    }
    else {
        $("#DivGstSection").css("display", "none");
    }
});

function checkUserIdStrength() {
    var thiscurrval = $("#txtMobileNo").val();
    $("#txtUserId").val(thiscurrval);
}

$("#btnStep1Next").click(function () {
    if (CheckFocusBlankValidation("txtFirstName")) return !1;
    if (CheckFocusBlankValidation("txtLastName")) return !1;
    if (CheckFocusBlankValidation("txtMobileNo")) return !1;
    if (CheckFocusBlankValidation("txtEmailId")) return !1;
    if (CheckEmailValidatoin("txtEmailId")) return !1;

    $("#step-1").css("display", "none");
    $("#step-2").css("display", "block");
    $(".step_no1").css({ "background": "#1ABB9C", "color": "#fff" });
    $('<style>#step_no1:before{background:#1ABB9C;color:#fff}</style>').appendTo('head');
    $('<style>#step_no2:before{background:#34495E;}</style>').appendTo('head');
    $(".step_no2").css({ "background": "#34495E", "color": "#fff" });
    GetStateList();
});
$("#btnStep2Next").click(function () {
    if (CheckFocusBlankValidation("txtAgencyName")) return !1;
    if (CheckFocusBlankValidation("txtAgencyAddress")) return !1;
    if (CheckFocusBlankValidation("ddlCountry")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlState")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlCity")) return !1;
    if (CheckFocusBlankValidation("txtPinCode")) return !1;
    if (CheckFocusBlankValidation("txtPanNo")) return !1;

    $("#txtPanNo").css("border", "1px solid #ced4da");
    if ($("#txtPanNo").hasClass("panerror")) {
        $("#txtPanNo").css("border", "1px solid #fd00007d");
        return false;
    }
    if (CheckFocusBlankValidation("txtNameOnPan")) return !1;

    $("#fluPanCard").css("border", "1px solid #ced4da");
    if ($("#fluPanCard").val() == "") {
        $("#fluPanCard").css("border", "1px solid #fd00007d");
        return false;
    }
    $("#fluCompAddress").css("border", "1px solid #ced4da");
    if ($("#fluCompAddress").val() == "") {
        $("#fluCompAddress").css("border", "1px solid #fd00007d");
        return false;
    }

    if ($("#chkIsGst").prop("checked") == true) {
        if (CheckFocusBlankValidation("txtGstNumber")) return !1;
        $("#txtGstNumber").css("border", "1px solid #ced4da");
        if ($("#txtGstNumber").hasClass("gstnoerror")) {
            $("#txtGstNumber").css("border", "1px solid #fd00007d");
            return false;
        }
        if (CheckFocusBlankValidation("txtGstCompanyName")) return !1;
        if (CheckFocusBlankValidation("txtGstCompanyAddress")) return !1;
        if (CheckFocusDropDownBlankValidation("ddlGstState")) return !1;
        if (CheckFocusDropDownBlankValidation("ddlGstCity")) return !1;
        if (CheckFocusBlankValidation("txtGstPinCode")) return !1;
        if (CheckFocusBlankValidation("txtGstEmailId")) return !1;
        if (CheckEmailValidatoin("txtGstEmailId")) return !1;
    }

    $("#step-2").css("display", "none");
    $("#step-3").css("display", "block");
    $('<style>#step_no2:before{background:#1ABB9C;color:#fff}</style>').appendTo('head');
    $(".step_no2").css({ "background": "#1ABB9C", "color": "#fff" });
    $(".step_no3").css({ "background": "#34495E", "color": "#fff" });
    $('<style>#step_no3:before{background:#34495E;}</style>').appendTo('head');
});
$("#btnStep2Prev").click(function () { $("#step-2").css("display", "none"); $("#step-1").css("display", "block"); });
$("#btnStep3Prev").click(function () { $("#step-3").css("display", "none"); $("#step-2").css("display", "block"); });

$("#btnSaveAgencyDetail").click(function () {
    if (CheckFocusBlankValidation("txtUserId")) return !1;
    if (CheckFocusBlankValidation("txtPassword")) return !1;

    var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,16}$/;
    if (!regex.test(document.getElementById("txtPassword").value)) {
        $('.auserpass').append("<span class='passwordincorrect text-danger pull-right' style='position: absolute;top: 85px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Password must contain:8 To 16 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character.</span>").css("display", "block");
        return false;
    }
    if (CheckFocusBlankValidation("txtConfPassword")) return !1;
    if ($('#txtPassword').val().trim() != $('#txtConfPassword').val().trim()) {
        $('.auserconfpass').append("<span class='confpasswordincorrect text-danger pull-right' style='position: absolute;top: 40px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Confirm Password is same as Password.</span>").css("display", "block");
        return false;
    }

    InsertAgencyDetail();
});

$("#createclose").click(function () {
    window.location.reload();
});

CheckPanNumber = () => {
    $("#txtPanNo").removeClass("panerror");
    $(".panerrmsg").remove();
    let panVal = $("#txtPanNo").val().toUpperCase();
    if (panVal != "" && panVal != undefined) {
        var regpan = /[a-zA-Z]{3}[PCHFATBLJG]{1}[a-zA-Z]{1}[0-9]{4}[a-zA-Z]{1}$/;
        if (regpan.test(panVal)) { $(".pancardno").removeClass("has-error"); return true; }
        else {
            $("#txtPanNo").addClass("panerror");
            $(".pancardno").append("<span class='panerrmsg text-danger pull-right' style='position: absolute;top: 40px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Permanent Account Number is not yet valid.</span>").css("display", "block");
            return false;
        }
    }
}

CheckGSTNumber = () => {
    $("#txtGstNumber").removeClass("gstnoerror");
    $(".gstnoerrmsg").remove();
    var inputvalues = $("#txtGstNumber").val().toUpperCase();
    var gstinformat = /^([0][1-9]|[1-2][0-9]|[3][0-7])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/;
    if (gstinformat.test(inputvalues)) {
        $("#txtGstNumber").css("border", "1px solid #ced4da");
        return true;
    } else {
        $("#txtGstNumber").addClass("gstnoerror");
        $(".gstnumber").append("<span class='gstnoerrmsg text-danger pull-right' style='position: absolute;top: 40px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> GST number is not yet valid.</span>").css("display", "block");
        return false;
    }
}

$("#fluPanCard").on("change", function (e) {
    const file = this.files[0];
    const fileType = file['type'];
    const validImageTypes = ['image/jpeg', 'image/jpg'];
    if (!validImageTypes.includes(fileType)) {
        $("#fluPanCard").val("");
    }
});
$("#fluCompAddress").on("change", function (e) {
    const file = this.files[0];
    const fileType = file['type'];
    const validImageTypes = ['image/jpeg', 'image/jpg'];
    if (!validImageTypes.includes(fileType)) {
        $("#fluCompAddress").val("");
    }
});
$("#fluAgencyLogo").on("change", function (e) {
    const file = this.files[0];
    const fileType = file['type'];
    const validImageTypes = ['image/jpeg', 'image/jpg', 'image/png'];
    if (!validImageTypes.includes(fileType)) {
        $("#fluAgencyLogo").val("");
    }
});

GetStateList = () => {
    $("#ddlState").prop("disabled", true);
    $("#ddlState").children().remove();
    $("#ddlState").append($('<option>').val(0).text('Please wait...'));

    $("#ddlGstState").prop("disabled", true);
    $("#ddlGstState").children().remove();
    $("#ddlGstState").append($('<option>').val(0).text('Please wait...'));

    $.ajax({
        type: "GET",
        url: "/Profile/GetStateList",
        contentType: "application/json; charset=utf-8",
        //data: { "country": "India" },
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $("#ddlState").children().remove();
                $("#ddlState").append($('<option>').val("").text('Select State'));
                $.each(data, function (i, data) {
                    $("#ddlState").append($('<option></option>').val(data.Value).html(data.Text));
                });
                $("#ddlState").prop("disabled", false);

                $("#ddlGstState").children().remove();
                $("#ddlGstState").append($('<option>').val("").text('Select State'));
                $.each(data, function (i, data) {
                    $("#ddlGstState").append($('<option></option>').val(data.Value).html(data.Text));
                });
                $("#ddlGstState").prop("disabled", false);
            }
            else {
                $("#ddlState").append($('<option>').val(0).text('Select State'));
                $("#ddlGstState").append($('<option>').val(0).text('Select State'));
            }
        }
    });
}

$(".openform").click(function () {
    $("#ddlTitle").val('Mr.');
    $("#txtFirstName").val("");
    $("#txtLastName").val("");
    $("#txtMobileNo").val("");
    $("#txtEmailId").val("");
    $("#txtWhatsupNo").val("");
    $("#txtAgencyName").val("");
    $("#txtAgencyAddress").val("");
    $("#ddlState").val("");
    $("#ddlCity").val("");
    $("#txtPinCode").val("");
    $("#txtPanNo").val("");
    $("#txtNameOnPan").val("");
    $('#chkIsGst').prop('checked', false);
    $("#fluPanCard").val("");
    $("#fluCompAddress").val("");
    $("#fluAgencyLogo").val("");
    $("#txtGstNumber").val("");
    $("#txtGstCompanyName").val("");
    $("#txtGstCompanyAddress").val("");
    $("#ddlGstState").val("");
    $("#ddlGstCity").val("");
    $("#txtGstPinCode").val("");
    $("#txtGstPhNumber").val("");
    $("#txtGstEmailId").val("");
    $("#txtUserId").val("");
    $("#txtPassword").val("");
    $("#txtConfPassword").val("");
});

function InsertAgencyDetail() {
    var model = {};

    model.Title = $("#ddlTitle option:selected").val();
    model.Fname = $("#txtFirstName").val();
    model.Lname = $("#txtLastName").val();
    model.Mobile = $("#txtMobileNo").val();
    model.Email = $("#txtEmailId").val();
    model.WhMobile = $("#txtWhatsupNo").val();

    model.Agency_Name = $("#txtAgencyName").val();
    model.Address = $("#txtAgencyAddress").val();
    model.Country = $("#ddlCountry option:selected").val();
    model.State = $("#ddlState option:selected").val();
    model.City = $("#ddlCity option:selected").val();
    model.zipcode = $("#txtPinCode").val();
    model.PanNo = $("#txtPanNo").val();
    model.NamePanCard = $("#txtNameOnPan").val();

    //file upload

    model.GSTNO = $("#txtGstNumber").val();
    if (model.GSTNO != "") { model.Is_GST_Apply = "1"; }
    else { model.Is_GST_Apply = "0"; }
    model.GST_Company_Name = $("#txtGstCompanyName").val();
    model.GST_Company_Address = $("#txtGstCompanyAddress").val();
    model.GST_PhoneNo = $("#txtGstPhNumber").val();
    model.GST_Email = $("#txtGstEmailId").val();
    model.GST_Pincode = $("#txtGstPinCode").val();
    model.GST_State = $("#ddlGstState option:selected").val();
    model.GST_City = $("#ddlGstCity option:selected").val();

    model.User_Id = $("#txtUserId").val();
    model.PWD = $("#txtPassword").val();

    $("#btnSaveAgencyDetail").html("Please Wait...<i class='fa fa-spinner fa-pulse'></i>");
    $.ajax({
        type: "Post",
        url: "/Profile/InsertAgencyRegistration",
        contentType: "application/json; charset=utf-8",
        data: '{model: ' + JSON.stringify(model) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data == true) {
                    $("#CreateAgencyBody").html("").html("<div style='text-align: center;'><p><i class='fa fa-check-circle text-success' aria-hidden='true' style='font-size: 100px;'></i><br></p><h4 class='text-success' style='font-size:20px;'>Agency created successfully.</h4><p></p></div>")
                }
                else {
                    alert("Failed !");
                }

                $("#btnSaveAgencyDetail").html("Save Detail");
            }
        }
    });    
}

