﻿using AdminPanel.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace AdminPanel.DataBase
{
    public class UploadDatabase
    {
        #region [Common : Connect to database]                
        private static SqlConnection conString = new SqlConnection(ConfigFile.DatabaseConnectionString);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Closed)
            {
                constr.Open();
            }
        }
        public static void CloseConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Open)
            {
                constr.Close();
            }
        }
        #endregion

        #region[Bank Details]
        internal static DataTable GetBankDetails()
        {
            try
            {

                Command = new SqlCommand("Bank_Accounts", conString);
                Command.CommandType = CommandType.StoredProcedure;

                Command.Parameters.AddWithValue("@Action", "SELECT");
                Command.Parameters.AddWithValue("@BankName", "SELECT");
                Command.Parameters.AddWithValue("@BranchName", "SELECT");
                Command.Parameters.AddWithValue("@Area", "SELECT");
                Command.Parameters.AddWithValue("@AccountNumber", "SELECT");
                Command.Parameters.AddWithValue("@NEFTCode", "SELECT");
                Command.Parameters.AddWithValue("@DISTRID", "SELECT");

                OpenConnection(conString);
                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "BankDetails");
                ObjDataTable = ObjDataSet.Tables["BankDetails"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return ObjDataTable;
        }
        internal static bool DeleteBankDetails(string id)
        {
            try
            {
                Command = new SqlCommand("Bank_Accounts", conString);
                Command.CommandType = CommandType.StoredProcedure;

                Command.Parameters.AddWithValue("@Action", "DELETE");
                Command.Parameters.AddWithValue("@Id", id);

                OpenConnection(conString);
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(conString);

                if (isSuccess == -1)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }

        internal static bool AddBankDetails(BankDetails model)
        {
            try
            {
                string query = "insert into BankDetails (BankName,	BranchName,	Area, AccountNumber, NEFTCode, DISTRID) values ('" + model.BankName + "','" + model.BranchName + "', '" + model.Area + "','" + model.AccountNumber + "', '" + model.NEFTCode + "','" + model.DISTRID + "')";
                Command = new SqlCommand(query, conString);
                Command.CommandType = CommandType.Text;

                OpenConnection(conString);
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(conString);

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        #endregion

        #region[Agency Credit And Debit]
        internal static DataTable AgencyStokistList_ddl()
        {
            try
            {

                Command = new SqlCommand("ChangeStockList_DI", conString);
                Command.CommandType = CommandType.StoredProcedure;

                Command.Parameters.AddWithValue("@Case", "All");

                OpenConnection(conString);
                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "AgencyStokistList");
                ObjDataTable = ObjDataSet.Tables["AgencyStokistList"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return ObjDataTable;
        }
        #endregion

        #region[Agency Credit Limit]
        internal static DataTable AgencyList()
        {
            try
            {
                Command = new SqlCommand("SELECT Counter,Agency_Name,User_Id,Agent_Type,TDS,ExmptTDS,ExmptTdsLimit FROM agent_register", conString);
                Command.CommandType = CommandType.Text;
                Adapter = new SqlDataAdapter();
                OpenConnection(conString);
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "AgencyList");
                ObjDataTable = ObjDataSet.Tables["AgencyList"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return ObjDataTable;
        }
        internal static DataTable AgencyDetail(string userid)
        {
            try
            {
                Command = new SqlCommand("SELECT * FROM agent_register where User_Id ='"+ userid + "'", conString);
                Command.CommandType = CommandType.Text;
                Adapter = new SqlDataAdapter();
                OpenConnection(conString);
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "AgencyDetail");
                ObjDataTable = ObjDataSet.Tables["AgencyDetail"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return ObjDataTable;
        }
        internal static bool UpdateAgencyCreditLimit(string id, string creditlimit, string expirydate, string remarks)
        {
            try   
            {
                //Command = new SqlCommand("SP_SET_AGENTCREDITLIMIT", conString);
                //Command.CommandType = CommandType.StoredProcedure;
                //Command.Parameters.AddWithValue("@UserId", UserId);
                //Command.Parameters.AddWithValue("@AgencyId", AgencyId);
                //Command.Parameters.AddWithValue("@AgentCredit", Convert.ToDouble(creditlimit));
                //Command.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UID"]));
                //Command.Parameters.AddWithValue("@ActionType", "CREDIT");
                //Command.Parameters.AddWithValue("@IPAddress", ipaddress);
                //Command.Parameters.AddWithValue("@Remark", Remark);
                //Command.Parameters.AddWithValue("@InvoiceNo", InvoiceNo);
                //Command.Parameters.AddWithValue("@LimitExpiryDate", LimitExpiryDate);
                //Command.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
                //Command.Parameters["@Msg"].Direction = ParameterDirection.Output;
                //OpenConnection(conString);
                //int rows = Command.ExecuteNonQuery();
                //CloseConnection(conString);
                //string msgout = Command.Parameters["@Msg"].Value.ToString();

                //if (rows > 0)
                //{
                //    return true;
                //}
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        #endregion
    }
}